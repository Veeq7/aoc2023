#!/bin/python

def get_data(file):
    lines = [line.strip() for line in file.readlines()]

    data = []
    for line in lines:
        pass

    return data

def solution_a(file):
    data = get_data(file)

    result = 0
    for _ in data:
        pass

    return result

def solution_b(file):
    data = get_data(file)

    result = 0
    for _ in data:
        pass

    return result

def run(solver, filename):
    print(f"{solver.__name__} {filename}")
    with open(filename, "r") as file:
        result = solver(file)
    print(f"{result}")
    print()

if __name__ == "__main__":
    run(solution_a, "sample.txt")
    run(solution_a, "input.txt")
    run(solution_b, "sample.txt")
    run(solution_b, "input.txt")
