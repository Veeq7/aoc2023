#!/bin/python

TOP    = 1 << 0
BOTTOM = 1 << 1
LEFT   = 1 << 2
RIGHT  = 1 << 3

opposite_of = {
    LEFT: RIGHT,
    RIGHT: LEFT,
    TOP: BOTTOM,
    BOTTOM: TOP,
}

direction_map = {
    ".": 0,
    "S": TOP | BOTTOM | LEFT | RIGHT,
    "|": TOP | BOTTOM,
    "-": LEFT | RIGHT,
    "L": TOP | RIGHT,
    "J": TOP | LEFT,
    "7": BOTTOM | LEFT,
    "F": BOTTOM | RIGHT,
}

def get_data(file):
    grid = {}
    char_grid = []
    start_coords = ()

    y = 0
    lines = file.readlines()
    for line in lines:
        x = 0
        row = []
        for c in line:
            if not c in direction_map:
                continue
            grid[(x, y)] = direction_map[c]
            row.append(c)
            if c == "S":
                start_coords = (x, y)
            x += 1
        char_grid.append(row)
        y += 1
    
    return grid, char_grid, start_coords

def maybe_move(out, grid, cursor, new_cursor, direction):
    if grid[cursor] & direction and grid.get(new_cursor, 0) & opposite_of[direction]:
        out.add(new_cursor)
        return True
    return False

def solution_a(file):
    grid, _, start_coords = get_data(file)

    steps = 0
    cursors = {start_coords} if start_coords else {}
    while cursors:
        new_cursors = set()
        for x, y in cursors:
            maybe_move(new_cursors, grid, (x, y), (x - 1, y), LEFT)
            maybe_move(new_cursors, grid, (x, y), (x + 1, y), RIGHT)
            maybe_move(new_cursors, grid, (x, y), (x, y - 1), TOP)
            maybe_move(new_cursors, grid, (x, y), (x, y + 1), BOTTOM)
            del grid[(x, y)]
        cursors = new_cursors
        if cursors:
            steps += 1
    return steps

# This one was tough, had to watch https://www.youtube.com/watch?v=r3i3XE9H4uw
# feels like part 1 and part 2 were completely different, so the abstractions I chose
# were more getting in the way instead of helpful
def solution_b(file):
    grid, char_grid, start_coords = get_data(file)

    simple_grid = [["." for _ in row] for row in char_grid]

    loop = set()
    cursors = {start_coords} if start_coords else {}
    while cursors:
        new_cursors = set()
        for x, y in cursors:
            loop.add((x, y))
            states = 0
            if maybe_move(new_cursors, grid, (x, y), (x - 1, y), LEFT):
                states |= LEFT
            if maybe_move(new_cursors, grid, (x, y), (x + 1, y), RIGHT):
                states |= RIGHT
            if maybe_move(new_cursors, grid, (x, y), (x, y - 1), TOP):
                states |= TOP
            if maybe_move(new_cursors, grid, (x, y), (x, y + 1), BOTTOM):
                states |= BOTTOM

            c = char_grid[y][x]
            if c == "S":
                for ch, value in direction_map.items():
                    if states == value:
                        c = ch
                        break
            simple_grid[y][x] = c

            del grid[(x, y)]
        cursors = new_cursors

    outside = set()
    for y, row in enumerate(simple_grid):
        within = False
        up = None
        for x, ch in enumerate(row):
            if ch == "|":
                assert up is None
                within = not within
            elif ch == "-":
                assert up is not None
            elif ch in "LF":
                assert up is None
                up = ch == "L"
            elif ch in "7J":
                assert up is not None
                if ch != ("J" if up else "7"):
                    within = not within
                up = None
            elif ch in ".":
                pass
            else:
                assert False
            if not within:
                outside.add((x, y))

    return len(simple_grid) * len(simple_grid[0]) - len(loop | outside) 

def run(solver, filename):
    print(f"{solver.__name__} {filename}")
    with open(filename, "r") as file:
        result = solver(file)
    print(f"{result}")
    print()

if __name__ == "__main__":
    run(solution_a, "sample.txt")
    run(solution_a, "input.txt")
    run(solution_b, "sample.txt")
    run(solution_b, "sample2.txt")
    run(solution_b, "input.txt")
