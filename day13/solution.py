#!/bin/python
from copy import deepcopy

def get_data(file):
    lines = [line.strip() for line in file.readlines()]

    patterns = []
    rows = []
    for line in lines + [""]:
        if not line:
            width = max([len(row) for row in rows]) if rows else 0
            cols = ["" for _ in range(width)]
            for row in rows:
                for c, ch in enumerate(row):
                    cols[c] += ch

            patterns.append((rows, cols))
            rows = []
        else:
            rows.append(line)
    return patterns

def solve(array, ignore=-1):
    for i, line in enumerate(array[:-1]):
        if i == ignore:
            continue
        n = 0
        l = i
        r = i + 1
        while l >= 0:
            left = array[l]
            right = array[r] if r < len(array) else left
            if left != right:
                n = 0
                break

            n += 1
            l -= 1
            r += 1
        if n:
            return n, i
    return 0, 0

def solve_either(rows, cols):
    row_n, i = solve(rows)
    if row_n:
        return row_n * 100, i, -1
    else:
        col_n, i = solve(cols)
        return col_n, -1, i

def solve_new(arr, i):
    for r, row in enumerate(arr):
        for c, ch in enumerate(row):
            copy = deepcopy(arr)

            l = list(copy[r])
            l[c] = "." if ch == "#" else "#"
            copy[r] = "".join(l)

            n, _ = solve(copy, ignore=i)
            if n:
                return n
    return 0


def solution_a(file):
    patterns = get_data(file)
    
    result = 0
    for rows, cols in patterns:
        result += solve_either(rows, cols)[0]

    return result

def solution_b(file):
    patterns = get_data(file)
    
    result = 0
    for rows, cols in patterns:
        _, i, j = solve_either(rows, cols)
        result += solve_new(rows, i) * 100 or solve_new(cols, j)

    return result

def run(solver, filename):
    print(f"{solver.__name__} {filename}")
    with open(filename, "r") as file:
        result = solver(file)
    print(f"{result}")
    print()

if __name__ == "__main__":
    run(solution_a, "sample.txt")
    run(solution_a, "input.txt")
    run(solution_b, "sample.txt")
    run(solution_b, "input.txt")
