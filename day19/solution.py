#!/bin/python
import re

class Ruleset:
    def __init__(self):
        self.name = ""
        self.rules = []
        self.fallback = ""

class Rule:
    def __init__(self):
        self.arg = ""
        self.operator = ""
        self.comparand = 0
        self.outcome = ""

def get_data(file):
    lines = [line.strip() for line in file.readlines()]

    rulesets = {}
    workflows = []

    parsing_rules = True
    for line in lines:
        if not line:
            parsing_rules = False
            continue

        if parsing_rules:
            name, rhs = re.match(r"([a-z]+)(.*)", line).groups()
            items = rhs.removeprefix("{").removesuffix("}").split(",")
            
            ruleset = Ruleset()
            ruleset.name = name
            ruleset.fallback = items[-1]
            for item in items[:-1]:
                arg, operator, comparand, outcome = re.match(r"([a-z]+)([<>])([0-9]+):([a-zAR]+)", item).groups()

                rule = Rule()
                rule.arg = arg
                rule.operator = operator
                rule.comparand = int(comparand)
                rule.outcome = outcome
                ruleset.rules.append(rule)

            rulesets[name] = ruleset
        else:
            items = line.removeprefix("{").removesuffix("}").split(",")

            workflow = {}
            for item in items:
                key, value = item.split("=")
                workflow[key] = int(value)
            workflows.append(workflow)
    
    return rulesets, workflows

def check_rule(rule, workflow):
    value = workflow[rule.arg]
    if rule.operator == "<":
        return value < rule.comparand
    elif rule.operator == ">":
        return value > rule.comparand

    assert False

def step(rulesets, workflow, ruleset):
    def resolve(name):
        if name == "A":
            return True, None
        elif name == "R":
            return False, None
        else:
            return False, rulesets[name]

    for rule in ruleset.rules:
        if check_rule(rule, workflow):
            return resolve(rule.outcome)
    return resolve(ruleset.fallback)

def solution_a(file):
    rulesets, workflows = get_data(file)
    if not rulesets or not workflows:
        return 0

    result = 0
    for workflow in workflows:
        ruleset = rulesets["in"]
        accepted = False
        while ruleset:
            accepted, ruleset = step(rulesets, workflow, ruleset)
        if accepted:
            for val in workflow.values():
                result += val

    return result

def check_multirule(rule, ranges):
    if not ranges:
        return None, None

    lo, hi = ranges[rule.arg]
    if rule.comparand < lo or rule.comparand > hi:
        return None, None

    lhs = dict(ranges)
    rhs = dict(ranges)
    if rule.operator == "<":
        lhs[rule.arg] = (lo, rule.comparand - 1)
        rhs[rule.arg] = (rule.comparand, hi)
    elif rule.operator == ">":
        lhs[rule.arg] = (rule.comparand + 1, hi)
        rhs[rule.arg] = (lo, rule.comparand)

    return lhs, rhs

def ranges_len(ranges):
    result = 1
    for lo, hi in ranges.values():
        result *= hi - lo + 1
    return result

def multistep(rulesets, ruleset, ranges):
    def resolve(name, r):
        if not r:
            return 0

        if name == "A":
            return ranges_len(r)
        elif name == "R":
            return 0
        else:
            return multistep(rulesets, rulesets[name], r)

    total = 0
    for rule in ruleset.rules:
        value, ranges = check_multirule(rule, ranges)
        total += resolve(rule.outcome, value)
    total += resolve(ruleset.fallback, ranges)
    return total

def solution_b(file):
    rulesets, _ = get_data(file)
    if not rulesets:
        return 0

    ruleset = rulesets["in"]
    return multistep(rulesets, ruleset, {key: (1, 4000) for key in "xmas"})

def run(solver, filename):
    print(f"{solver.__name__} {filename}")
    with open(filename, "r") as file:
        result = solver(file)
    print(f"{result}")
    print()

if __name__ == "__main__":
    run(solution_a, "sample.txt")
    run(solution_a, "input.txt")
    run(solution_b, "sample.txt")
    run(solution_b, "input.txt")
