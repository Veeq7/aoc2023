#!/bin/python
import enum
import math
from heapq import heappush, heappop

class Dir(enum.IntEnum):
    LEFT  = enum.auto()
    RIGHT = enum.auto()
    DOWN  = enum.auto()
    UP    = enum.auto()

moves = {
    Dir.LEFT:  (-1,  0),
    Dir.RIGHT: ( 1,  0),
    Dir.DOWN:  ( 0,  1),
    Dir.UP:    ( 0, -1),
}

opposite_of = {
    Dir.LEFT:  Dir.RIGHT,
    Dir.RIGHT: Dir.LEFT,
    Dir.DOWN:  Dir.UP,
    Dir.UP:    Dir.DOWN,
}

def get_data(file):
    grid = [list(map(int, line.strip())) for line in file.readlines()]
    return grid

def add_move(grid, to_visit, hl, x, y, dir, dir_reps):
    move = moves[dir]
    nx = x + move[0]
    ny = y + move[1]

    w, h = len(grid[0]), len(grid)
    if 0 <= nx < w and 0 <= ny < h:
        heappush(to_visit, (hl + grid[ny][nx], (nx, ny), dir, dir_reps))

def solve(grid, is_part2):
    w, h = len(grid[0]), len(grid)
    seen = set()
    to_visit = [(0, (0, 0), None, 0)]

    while to_visit:
        hl, (x, y), last_dir, dir_reps = heappop(to_visit)
        if x == w - 1 and y == h - 1:
            if not is_part2 or dir_reps >= 4:
                return hl

        key = (x, y, last_dir, dir_reps)
        if key in seen:
            continue
        seen.add(key)

        if is_part2:
            if dir_reps < 10 and last_dir is not None:
                add_move(grid, to_visit, hl, x, y, last_dir, dir_reps + 1)
            if dir_reps >= 4 or last_dir is None:
                for dir in Dir:
                    if dir == last_dir or opposite_of[dir] == last_dir:
                        continue
                    add_move(grid, to_visit, hl, x, y, dir, 1)
        else:
            if dir_reps < 3 and last_dir is not None:
                add_move(grid, to_visit, hl, x, y, last_dir, dir_reps + 1)
            for dir in Dir:
                if dir == last_dir or opposite_of[dir] == last_dir:
                    continue
                add_move(grid, to_visit, hl, x, y, dir, 1)

    return 0

def solution_a(file):
    grid = get_data(file)
    if not grid:
        return 0
    return solve(grid, False)

def solution_b(file):
    grid = get_data(file)
    if not grid:
        return 0
    return solve(grid, True)

def run(solver, filename):
    print(f"{solver.__name__} {filename}")
    with open(filename, "r") as file:
        result = solver(file)
    print(f"{result}")
    print()

if __name__ == "__main__":
    run(solution_a, "sample.txt")
    run(solution_a, "input.txt")
    run(solution_b, "sample.txt")
    run(solution_b, "sample2.txt")
    run(solution_b, "input.txt")
