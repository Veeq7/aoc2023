#!/bin/python

import re
import math

def solution_a(lines):
    if not len(lines):
        return 0

    moves = lines[0].strip()
    nodes = {}
    for line in lines[2:]:
        key, left, right = re.match(r"([A-Z]{3}) = \(([A-Z]{3}), ([A-Z]{3})\)", line).groups()
        nodes[key] = (left, right)

    current = "AAA"
    goal = "ZZZ"

    steps = 0
    while current != goal:
        move = moves[steps % len(moves)]
        
        left, right = nodes[current]
        if move == "L":
            current = left
        elif move == "R":
            current = right
        else:
            assert(False)
        
        steps += 1

    return steps

def solution_b(lines):
    if not len(lines):
        return 0

    moves = lines[0].strip()
    nodes = {}
    for line in lines[2:]:
        key, left, right = re.match(r"([A-Z0-9]{3}) = \(([A-Z0-9]{3}), ([A-Z0-9]{3})\)", line).groups()
        nodes[key] = (left, right)
    
    lcm_list = []
    for current in nodes.keys():
        if current[2] == "A":
            steps = 0
            while current[2] != "Z":
                move = moves[steps % len(moves)]
                
                left, right = nodes[current]
                if move == "L":
                    current = left
                elif move == "R":
                    current = right
                else:
                    assert(False)

                steps += 1
            lcm_list.append(steps)
                
    result = 1
    for lcm in lcm_list:
        result = math.lcm(result, lcm)
    return result

def run(solver, filename):
    with open(filename, "r") as file:
        lines = file.readlines()

    print(f"{solver.__name__} {filename}")
    result = solver(lines)
    print(f"{result}")
    print()

if __name__ == "__main__":
    run(solution_a, "sample.txt")
    run(solution_a, "sample2.txt")
    run(solution_a, "input.txt")
    run(solution_b, "sample3.txt")
    run(solution_b, "input.txt")
