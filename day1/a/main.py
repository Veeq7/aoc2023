#!/bin/python

with open("input.txt", "r") as file:
    lines = file.readlines()

result = 0
for line in lines:   
    first = None
    last = None
    for char in line:
        if char.isdigit():
            if first is None:
                first = char
            last = char

    if first and last:
        result += int(first + last)        

print(result)