#!/bin/python

digits = {
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9"
}

with open("input.txt", "r") as file:
    lines = file.readlines()

result = 0
for line in lines:
    first = None
    last = None
    while line:
        char = None
        if line[0].isdigit():
            char = line[0]
        else:
            for name, digit in digits.items():
                if line.startswith(name):
                    char = digit
                    break
        line = line[1:]

        if char is not None:
            if first is None:
                first = char
            last = char

    if first and last:
        result += int(first + last)

print(result)