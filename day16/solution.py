#!/bin/python
import enum
import copy

class Dir(enum.Enum):
    LEFT  = enum.auto()
    RIGHT = enum.auto()
    DOWN  = enum.auto()
    UP    = enum.auto()

moves = {
    Dir.LEFT:  (-1,  0),
    Dir.RIGHT: ( 1,  0),
    Dir.DOWN:  ( 0,  1),
    Dir.UP:    ( 0, -1),
}

split_map = {
    Dir.LEFT: {
        "/":  [Dir.DOWN],
        "\\": [Dir.UP],
        "|":  [Dir.UP, Dir.DOWN],
        "-":  [Dir.LEFT],
        ".":  [Dir.LEFT],
    },
    Dir.RIGHT: {
        "/":  [Dir.UP],
        "\\": [Dir.DOWN],
        "|":  [Dir.UP, Dir.DOWN],
        "-":  [Dir.RIGHT],
        ".":  [Dir.RIGHT],
    },
    Dir.UP: {
        "/":  [Dir.RIGHT],
        "\\": [Dir.LEFT],
        "|":  [Dir.UP],
        "-":  [Dir.LEFT, Dir.RIGHT],
        ".":  [Dir.UP],
    },
    Dir.DOWN: {
        "/":  [Dir.LEFT],
        "\\": [Dir.RIGHT],
        "|":  [Dir.DOWN],
        "-":  [Dir.LEFT, Dir.RIGHT],
        ".":  [Dir.DOWN],
    },
}

def get_data(file):
    board = [[ch for ch in line.strip()] for line in file.readlines()]
    return board

def solve(board, initial_beam):
    w, h = len(board[0]), len(board)
    beams = [initial_beam]
    energized = [["." for _ in row] for row in board]
    visited = set()
    while beams:
        new_beams = []
        for x, y, dir in beams:
            if (x, y, dir) in visited:
                continue
            visited.add((x, y, dir))
            energized[y][x] = "#"

            ch = board[y][x]
            for split in split_map[dir][ch]:
                move = moves[split]
                nx = x + move[0]
                ny = y + move[1]
                if nx < 0 or nx >= w or ny < 0 or ny >= h:
                    continue
                new_beams.append((nx, ny, split)) 
        beams = new_beams

    result = 0
    for row in energized:
        for ch in row:
            if ch == "#":
                result += 1
    return result

def solution_a(file):
    board = get_data(file)
    if not board:
        return 0
    
    return solve(board, (0, 0, Dir.RIGHT))

def solution_b(file):
    board = get_data(file)
    if not board:
        return 0

    result = 0
    w, h = len(board[0]), len(board)
    for x in range(w):
        result = max(result, solve(board, (x, 0,     Dir.DOWN)))
        result = max(result, solve(board, (x, h - 1, Dir.UP)))
    for y in range(h):
        result = max(result, solve(board, (0,     y, Dir.RIGHT)))
        result = max(result, solve(board, (w - 1, y, Dir.LEFT)))
    return result

def run(solver, filename):
    print(f"{solver.__name__} {filename}")
    with open(filename, "r") as file:
        result = solver(file)
    print(f"{result}")
    print()

if __name__ == "__main__":
    run(solution_a, "sample.txt")
    run(solution_a, "input.txt")
    run(solution_b, "sample.txt")
    run(solution_b, "input.txt")
