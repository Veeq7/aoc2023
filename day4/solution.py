#!/bin/python

def solution_a(lines):
    result = 0

    for line in lines:
        card, numbers = line.split(":")
        text, card_id = card.split()
        winning, game = [half.strip().split() for half in numbers.split("|")]

        local_sum = 0
        for num in game:
            if num in winning:
                if local_sum:
                    local_sum *= 2
                else:
                    local_sum = 1
        result += local_sum

    return result

def solution_b(lines):
    result = 0

    scratchcards = {}
    play_queue = []
    for line in lines:
        card, numbers = line.split(":")
        text, card_id = card.split()
        card_id = int(card_id)
        winning, game = [half.strip().split() for half in numbers.split("|")]

        scratchcards[card_id] = [winning, game]
        play_queue.append(card_id)

    for card_id in play_queue:
        winning, game = scratchcards[card_id]
        result += 1

        cards_won = 0
        for num in game:
            if num in winning:
                cards_won += 1

        for i in range(cards_won):
            play_queue.append(card_id + 1 + i)

    return result

def run(solver, filename):
    with open(filename, "r") as file:
        lines = file.readlines()

    print(f"{solver.__name__} {filename}")
    result = solver(lines)
    print(f"{result}")
    print()

if __name__ == "__main__":
    run(solution_a, "sample.txt")
    run(solution_a, "input.txt")
    run(solution_b, "sample.txt")
    run(solution_b, "input.txt")
