#!/bin/python

def get_values_list(lines):
    values_list = []
    for line in lines:
        values_list.append([int(n) for n in line.split()])
    return values_list

def get_diffs(values):
    diffs = []
    current = values
    while current:
        if any([c != 0 for c in current]):
            diffs.append(current)
        else:
            break

        diff = []
        for i, num in enumerate(current[:-1]):
            diff.append(current[i+1] - num)
        current = diff
    
    return diffs

def extrapolate_next_value(diffs):
    last_increment = 0
    for diff in diffs:
        last_increment = diff[-1] + last_increment
    return last_increment

def solution_a(lines):
    values_list = get_values_list(lines)

    result = 0
    for values in values_list:
        diffs = get_diffs(values)
        result += extrapolate_next_value(diffs)    

    return result

def solution_b(lines):
    values_list = get_values_list(lines)

    result = 0
    for values in values_list:
        values.reverse()
        diffs = get_diffs(values)
        result += extrapolate_next_value(diffs)    

    return result

def run(solver, filename):
    with open(filename, "r") as file:
        lines = file.readlines()

    print(f"{solver.__name__} {filename}")
    result = solver(lines)
    print(f"{result}")
    print()

if __name__ == "__main__":
    run(solution_a, "sample.txt")
    run(solution_a, "input.txt")
    run(solution_b, "sample.txt")
    run(solution_b, "input.txt")
