#!/bin/python
import copy
from collections import defaultdict

def get_data(file):
    return [[c for c in line.strip()] for line in file.readlines()]

def get_balls(rows):
    balls = []
    for r, row in enumerate(rows):
        for c, ch in enumerate(row):
            if ch == "O":
                balls.append((r, c))
    return balls

memo = {}

def tilt(rows, dc, dr):
    key = (tuple([tuple(row) for row in rows]), dc, dr)
    if key in memo:
        return memo[key]

    new_rows = copy.deepcopy(rows)
    for r, c in get_balls(new_rows):
        new_rows[r][c] = "."
        vr, vc = r, c
        while True:
            r += dr
            if r < 0 or r >= len(new_rows):
                break
            c += dc
            if c < 0 or c >= len(new_rows[0]):
                break
            if new_rows[r][c] == ".":
                vr, vc = r, c
            if new_rows[r][c] == "#":
                break
        new_rows[vr][vc] = "O"
    
    memo[key] = (new_rows, True)
    return (new_rows, False)

def solve(rows):
    load = 0
    h = len(rows)
    for r, row in enumerate(rows):
        for c, ch in enumerate(row):
            if rows[r][c] == "O":
                load += h - r
    return load

def solution_a(file):
    rows = get_data(file)
    result = solve(tilt(rows, 0, -1)[0])
    return result

def solution_b(file):
    rows = get_data(file)

    cycles = [(0, -1), (-1, 0), (0, 1), (1, 0)]
    n = 1000000000
    by_score = defaultdict(list)
    
    result = 0
    i = 0
    while i < n:
        i += 1
        for j in range(4):
            cycle = cycles[j % len(cycles)]
            rows, from_cache = tilt(rows, *cycle)
        
        result = solve(rows)
        by_score[result].append(i)

        timestamps = by_score[result]
        if len(timestamps) >= 6:
            cycle_size = timestamps[-1] - timestamps[-2]
            if cycle_size == timestamps[-2] - timestamps[-3]: 
                amount = (n - i) // cycle_size
                i += amount * cycle_size

    return result

def run(solver, filename):
    print(f"{solver.__name__} {filename}")
    with open(filename, "r") as file:
        result = solver(file)
    print(f"{result}")
    print()

if __name__ == "__main__":
    run(solution_a, "sample.txt")
    run(solution_a, "input.txt")
    run(solution_b, "sample.txt")
    run(solution_b, "input.txt")
