#!/bin/python
from collections import deque
import math

class Broadcaster:
    def __init__(self, name, outputs):
        self.name = name
        self.outputs = outputs
        self.enabled = False

    def pulse(self, prev, is_high, queue):
        for output in self.outputs:
            queue.append((output, self.name, is_high))

    def __repr__(self):
        return f"{{outputs: {self.outputs}}}"

class FlipFlop:
    def __init__(self, name, outputs):
        self.name = name
        self.outputs = outputs
        self.on = False
    
    def pulse(self, prev, is_high, queue):
        if not is_high:
            self.on = not self.on
            for output in self.outputs:
                queue.append((output, self.name, self.on))

    def __repr__(self):
        return f"FlipFlop {{outputs: {self.outputs}, on: {self.on}}}"
    
class Conjuction:
    def __init__(self, name, outputs):
        self.name = name
        self.outputs = outputs
        self.pulses = {}

    def pulse(self, prev, is_high, queue):
        self.pulses[prev] = is_high
        is_high = not all(self.pulses.values())
        for output in self.outputs:
            queue.append((output, self.name, is_high))

    def __repr__(self):
        return f"Conjuction {{outputs: {self.outputs}, pulses: {self.pulses}}}"

def get_data(file):
    lines = [line.strip() for line in file.readlines()]

    modules = {}
    for line in lines:
        name, outputs_string = line.split(" -> ")
        outputs = outputs_string.split(", ")

        if name == "broadcaster":
            modules[name] = Broadcaster(name, outputs)
        else:
            type = name[0]
            name = name[1:]
            if type == "%":
                modules[name] = FlipFlop(name, outputs)
            elif type == "&":
                modules[name] = Conjuction(name, outputs)
            else:
                assert False

    for name, module in modules.items():
        for output in module.outputs:
            if output in modules and isinstance(modules[output], Conjuction):
                modules[output].pulses[name] = False

    return modules

def solution_a(file):
    modules = get_data(file)

    lo = 0
    hi = 0
    for _ in range(1000):
        queue = deque([("broadcaster", "button", False)])
        while queue:
            cur, prev, is_high = queue.popleft()

            if is_high:
                hi += 1
            else:
                lo += 1

            if cur in modules:
                module = modules[cur]
                module.pulse(prev, is_high, queue)

    return lo * hi

def solution_b(file):
    modules = get_data(file)

    (feed,) = [name for name, module in modules.items() if "rx" in module.outputs]

    cycle_lengths = {}
    seen = {name: 0 for name, module in modules.items() if feed in module.outputs}

    presses = 0
    while True:
        presses += 1
        queue = deque([("broadcaster", "button", False)])
        while queue:
            cur, prev, is_high = queue.popleft()

            if cur == feed and is_high:
                seen[prev] += 1

                if prev not in cycle_lengths:
                    cycle_lengths[prev] = presses
                else:
                    assert presses == seen[prev] * cycle_lengths[prev]

                if all(seen.values()):
                    return math.lcm(*cycle_lengths.values())

            if cur in modules:
                module = modules[cur]
                module.pulse(prev, is_high, queue)

    return 0

def run(solver, filename):
    print(f"{solver.__name__} {filename}")
    with open(filename, "r") as file:
        result = solver(file)
    print(f"{result}")
    print()

if __name__ == "__main__":
    run(solution_a, "sample.txt")
    run(solution_a, "sample2.txt")
    run(solution_a, "input.txt")
    run(solution_b, "input.txt")
