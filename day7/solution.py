#!/bin/python
from collections import defaultdict

def get_sets(lines):
    sets = []
    for line in lines:
        hand, bid = line.split()
        sets.append((hand, int(bid)))
    return sets

def get_card_index(card, jokers):
    if jokers:
        cards = "J23456789TQKA"
    else:
        cards = "23456789TJQKA"
    return cards.index(card)

def get_hand_type(hand, jokers):
    types = [ (5,), (4,), (3, 2), (3,), (2, 2), (2,), (1, 1, 1, 1, 1) ]

    num_jokers = 0
    counter = defaultdict(int)
    for c in hand:
        if c == "J" and jokers:
            num_jokers += 1
        else:
            counter[c] += 1

    keys = list(counter.keys())
    keys.sort(key=lambda k: (-counter[k], -get_card_index(k, jokers)))
    counts = [counter[key] for key in keys]
    if counts:
        counts[0] += num_jokers
    else:
        counts = [num_jokers]
    for ti, t in enumerate(types):
        try:
            valid = True
            for i, arg in enumerate(t):
                if counts[i] != arg:
                    valid = False
                    break
            if valid:
                return len(types) - ti 
        except IndexError:
            pass
    assert(False)
    return -1

def get_hand_score(hand, jokers):
    return (get_hand_type(hand, jokers), [get_card_index(card, jokers) for card in hand])

def sum_sets(sets):
    result = 0
    rank = 1
    for hand, bid in sets:
        result += bid * rank
        rank += 1
    return result

def solution_a(lines):
    sets = get_sets(lines)
    sets.sort(key=lambda s: get_hand_score(s[0], False))
    return sum_sets(sets)

def solution_b(lines):
    sets = get_sets(lines)
    sets.sort(key=lambda s: get_hand_score(s[0], True))
    return sum_sets(sets)

def run(solver, filename):
    with open(filename, "r") as file:
        lines = file.readlines()

    print(f"{solver.__name__} {filename}")
    result = solver(lines)
    print(f"{result}")
    print()

if __name__ == "__main__":
    run(solution_a, "sample.txt")
    run(solution_a, "input.txt")
    run(solution_b, "sample.txt")
    run(solution_b, "input.txt")
