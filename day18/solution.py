#!/bin/python
import re

dir_map = {
    "L": (-1,  0),
    "R": ( 1,  0),
    "D": ( 0,  1),
    "U": ( 0, -1),
}

def get_data(file):
    lines = [line.strip() for line in file.readlines()]

    edges = 0
    data = []
    for line in lines:
        dir, n, hex = re.match("([RDLU]) ([0-9]+) \(#([0-9a-f]{6})\)", line).groups()
        data.append((dir, int(n), hex))
        edges += 1

    return data

def solve(points, outline):
    area = 0
    for i in range(len(points)):
        area += points[i][0] * (points[i - 1][1] - points[(i + 1) % len(points)][1])
    area = abs(area) // 2
    pick = area - outline // 2 + 1
    return pick + outline

def solution_a(file):
    data = get_data(file)

    points = [(0, 0)]
    outline = 0
    for dir, n, _ in data:
        x, y = points[-1]
        dx, dy = dir_map[dir]
        nx = x + dx * n
        ny = y + dy * n
        points.append((nx, ny))
        outline += n

    assert(points[0] == points[-1])
    points = points[:-1]

    return solve(points, outline)

def solution_b(file):
    data = get_data(file)

    points = [(0, 0)]
    outline = 0
    for _, _, hex in data:
        dir = "RDLU"[int(hex[-1])]
        n = int(hex[:-1], 16)

        x, y = points[-1]
        dx, dy = dir_map[dir]
        nx = x + dx * n
        ny = y + dy * n
        points.append((nx, ny))
        outline += n

    assert(points[0] == points[-1])
    points = points[:-1]

    return solve(points, outline)

def run(solver, filename):
    print(f"{solver.__name__} {filename}")
    with open(filename, "r") as file:
        result = solver(file)
    print(f"{result}")
    print()

if __name__ == "__main__":
    run(solution_a, "sample.txt")
    run(solution_a, "input.txt")
    run(solution_b, "sample.txt")
    run(solution_b, "input.txt")
