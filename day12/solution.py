#!/bin/python

def get_data(file):
    lines = [line.strip() for line in file.readlines()]

    data = []
    for line in lines:
        pattern, nums = line.split()
        data.append((pattern, [int(num) for num in nums.split(",")]))

    return data

memo = {}

def solve(pattern, run, nums):
    if not pattern:
        if not nums and not run:
            return 1
        if len(nums) == 1 and run == nums[0]:
            return 1
        return 0

    key = (pattern, run, tuple(nums))
    if key in memo:
        return memo[key]

    result = 0
    wanted_run = nums[0] if nums else 0
    ch = pattern[0]
    if ch in ("#", "?") and run < wanted_run:
        result += solve(pattern[1:], run + 1, nums)
    if ch in (".", "?") and not run:
        result += solve(pattern[1:], 0, nums)
    if ch in (".", "?") and run and run == wanted_run:
        result += solve(pattern[1:], 0, nums[1:])

    memo[key] = result
    return result

def solution_a(file):
    data = get_data(file)

    result = 0
    for pattern, nums in data:
        result += solve(pattern, 0, nums)

    return result

def solution_b(file):
    data = get_data(file)

    new_data = []
    for pattern, nums in data:
        new_pattern = ""
        new_nums = []
        for i in range(5):
            new_pattern += pattern
            if i != 4:
                new_pattern += "?"

            for num in nums:
                new_nums.append(num)
        new_data.append((new_pattern, new_nums))
    data = new_data

    result = 0
    for pattern, nums in data:
        result += solve(pattern, 0, nums)

    return result

def run(solver, filename):
    print(f"{solver.__name__} {filename}")
    with open(filename, "r") as file:
        result = solver(file)
    print(f"{result}")
    print()

if __name__ == "__main__":
    run(solution_a, "sample.txt")
    run(solution_a, "input.txt")
    run(solution_b, "sample.txt")
    run(solution_b, "input.txt")
