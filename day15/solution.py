#!/bin/python
import re

def get_data(file):
    return file.read().strip().split(",")

def hash_string(string):
    current = 0
    for ch in string:
        current += ord(ch)
        current *= 17
        current %= 256
    return current

def solution_a(file):
    values = get_data(file)

    result = 0
    for value in values:
        result += hash_string(value)

    return result

def solution_b(file):
    values = get_data(file)

    result = 0
    boxes = [{} for _ in range(256)]
    for value in values:
        label, operator, num = re.match(r"([a-z]+)([-=])([0-9]*)", value).groups()

        index = hash_string(label)
        box = boxes[index]

        if "-" in value:
            if label in box:
                del box[label]
        else:
            box[label] = int(num)

    for b, box in enumerate(boxes):
        for s, slot in enumerate(box.values()):
            result += (b + 1) * (s + 1) * slot

    return result

def run(solver, filename):
    print(f"{solver.__name__} {filename}")
    with open(filename, "r") as file:
        result = solver(file)
    print(f"{result}")
    print()

if __name__ == "__main__":
    run(solution_a, "sample.txt")
    run(solution_a, "input.txt")
    run(solution_b, "sample.txt")
    run(solution_b, "input.txt")
