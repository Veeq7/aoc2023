#!/bin/python

with open("input.txt", "r") as file:
    lines = file.readlines()

result = 0

for line in lines:
    scores = {
        "red": 0,
        "green": 0,
        "blue": 0,
    }

    game, values = line.split(":")
    game_id = int(game.split()[1].removesuffix(":"))
    sets = values.split(";")
    for set_ in sets:
        games = set_.split(",")
        for game in games:
            count, key = game.split()
            scores[key.strip()] = max(scores[key.strip()], int(count.strip()))

    local_sum = 0
    for key, value in scores.items():
        if local_sum:
            local_sum *= value
        else:
            local_sum = value
    result += local_sum

print(result)

