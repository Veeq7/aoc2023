#!/bin/python

def get_data(lines):
    times = [int(num) for num in lines[0].split(":")[1].split()]
    distances = [int(num) for num in lines[1].split(":")[1].split()]
    
    data = []
    i = 0
    while i < len(times) and i < len(distances):
        data += [(times[i], distances[i])]
        i += 1

    return data

def solution_a(lines):
    result = 0

    data = get_data(lines)
    for time, distance in data:
        local_result = 0
        for i in range(1, time): # 0 and time are always 0
            score = i * (time - i)
            if score > distance:
                local_result += 1

        if result:
            result *= local_result
        else:
            result = local_result

    return result

def solution_b(lines):
    result = 0

    data = get_data(lines)
    time = ""
    distance = ""
    for t, d in data:
        time += str(t)
        distance += str(d)
    time = int(time)
    distance = int(distance)

    for i in range(1, time): # 0 and time are always 0
        score = i * (time - i)
        if score > distance:
            result += 1

    return result

def run(solver, filename):
    with open(filename, "r") as file:
        lines = file.readlines()

    print(f"{solver.__name__} {filename}")
    result = solver(lines)
    print(f"{result}")
    print()

if __name__ == "__main__":
    run(solution_a, "sample.txt")
    run(solution_a, "input.txt")
    run(solution_b, "sample.txt")
    run(solution_b, "input.txt")
