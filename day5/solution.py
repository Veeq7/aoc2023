#!/bin/python

def get_category(lines, i):
    name = ""
    numbers = []

    while i < len(lines):
        line = lines[i]
        if ":" in line:
            name, numbers_raw = line.split(":")

            nums_to_add = [int(n) for n in numbers_raw.split()]
            if nums_to_add:
                numbers += [nums_to_add]

            i += 1
            while i < len(lines):
                line = lines[i]
                if len(line.strip()) == 0:
                    break
                nums_to_add = [int(n) for n in line.split()]
                if nums_to_add:
                    numbers += [nums_to_add]
                i += 1
            break
        i += 1

    return name, numbers, i

def get_seeds(lines, i):
    _, current, i = get_category(lines, i)
    return current[0] if len(current) else [], i

def get_categories(lines, i):
    categories = []
    while i < len(lines):
        _, values, i = get_category(lines, i)
        categories.append(values)
    return categories, i

def solution_a(lines):
    i = 0
    seeds, i = get_seeds(lines, i)
    categories, i = get_categories(lines, i)

    current = seeds
    output = []
    for values in categories:
        for dest, source, size in values:
            source_end = source + size - 1
            for c in list(current):
                if c >= source and c <= source_end:
                    current.remove(c)
                    output.append(c - source + dest)
        for c in list(current):
            current.remove(c)
            output.append(c)

        current = output
        output = []

    return min(current) if current else 0

def chop(seed_range, chop_range):
    chopped = None
    remainder = []

    if seed_range[0] <= chop_range[1] and seed_range[1] >= chop_range[0]:
        chopped = (max(seed_range[0], chop_range[0]), min(seed_range[1], chop_range[1]))
        if seed_range[0] < chopped[0]:
            remainder.append((seed_range[0], chopped[0] - 1))
        if seed_range[1] > chopped[1]:
            remainder.append((chopped[1] + 1, seed_range[1]))
    else:
        remainder = [seed_range]

    return chopped, remainder

def solution_b(lines):
    i = 0
    seeds, i = get_seeds(lines, i)
    categories, i = get_categories(lines, i)

    current = []
    for i in range(0, len(seeds), 2):
        current += [(seeds[i], seeds[i] + seeds[i+1] - 1)]

    output = []
    for values in categories:
        for dest, source, size in values:
            chop_range = (source, source + size - 1)
            i = 0
            while i < len(current):
                seed_range = current[i]

                chopped, remainder = chop(seed_range, chop_range)
                if chopped:
                    current.remove(seed_range)
                    current += remainder
                    output.append((chopped[0] - source + dest, chopped[1] - source + dest))
                else:
                    i += 1
        for seed_range in list(current):
            current.remove(seed_range)
            output.append(seed_range)

        current = output
        output = []

    return min([c[0] for c in current]) if current else 0

def run(solver, filename):
    with open(filename, "r") as file:
        lines = file.readlines()

    print(f"{solver.__name__} {filename}")
    result = solver(lines)
    print(f"{result}")
    print()

if __name__ == "__main__":
    run(solution_a, "sample.txt")
    run(solution_a, "input.txt")
    run(solution_b, "sample.txt")
    run(solution_b, "input.txt")
