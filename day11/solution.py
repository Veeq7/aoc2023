#!/bin/python

def get_data(file, factor):
    lines = [line.strip() for line in file.readlines()]

    occupied_rows = set()
    occupied_cols = set()
    for r, row in enumerate(lines):
        for c, ch in enumerate(row):
            if ch != ".":
                occupied_rows.add(r)
                occupied_cols.add(c)
    
    free_rows = []
    free_cols = []
    for r in range(len(lines)):
        if r not in occupied_rows:
            free_rows.append(r)
    for c in range(len(lines[0])):
        if c not in occupied_cols:
            free_cols.append(c)

    coords = []
    for r, row in enumerate(lines):
        for c, ch in enumerate(row):
            if ch != "#":
                continue
            nr, nc = r, c
            for fr in free_rows:
                if fr < r:
                    nr += factor - 1
                else:
                    break
            for fc in free_cols:
                if fc < c:
                    nc += factor - 1
                else:
                    break
            coords.append((nr, nc))

    return coords

def solve(coords):
    result = 0
    for i, (x, y) in enumerate(coords):
        for j in range(i+1, len(coords)):
            ox, oy = coords[j]
            result += abs(x - ox) + abs(y - oy)
    return result

def solution_a(file):
    coords = get_data(file, 2)
    return solve(coords)

def solution_sample_b(file):
    coords = get_data(file, 100)
    return solve(coords)

def solution_b(file):
    coords = get_data(file, 1000000)
    return solve(coords)

def run(solver, filename):
    print(f"{solver.__name__} {filename}")
    with open(filename, "r") as file:
        result = solver(file)
    print(f"{result}")
    print()

if __name__ == "__main__":
    run(solution_a, "sample.txt")
    run(solution_a, "input.txt")
    run(solution_sample_b, "sample.txt")
    run(solution_b, "input.txt")
