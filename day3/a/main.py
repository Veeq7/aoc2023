#!/bin/python

with open("input.txt", "r") as file:
    lines = file.readlines()

result = 0

for y in range(len(lines)):
    line = lines[y].strip()

    x = 0
    while x < len(line):
        char = line[x]
        start = x
        x += 1

        if char == "." or not char.isdigit():
            continue
        number = char

        while x < len(line):
            char = line[x]
            if char.isdigit():
                x += 1
                number += char
            else:
                break

        end = x
        number = int(number)

        checked = False
        def check_number(x, y):
            global checked
            if checked:
                return True
            try:
                check = lines[y].strip()[x]
                if not check.isdigit() and check != ".":
                    global result
                    result += number
                    checked = True
                    return True
            except IndexError:
                pass
            return False
        
        check_number(start - 1, y)
        check_number(end, y)
        for xx in range(start - 1, end + 1):
            if check_number(xx, y - 1):
                break
            if check_number(xx, y + 1):
                break

print(result)
