#!/bin/python

with open("input.txt", "r") as file:
    lines = file.readlines()

result = 0

for y in range(len(lines)):
    line = lines[y].strip()

    for x in range(len(line)):
        char = line[x]
        if char != "*":
            continue
         
        def find_numbers(sx, ex, y):
            try:
                line = lines[y].strip()
            except IndexError:
                return []

            exclude_list = set()
            def check(x):
                if x in exclude_list:
                    return False
                try:
                    char = line[x]
                    exclude_list.add(x)
                    return char.isdigit()
                except IndexError:
                    pass
                return False

            numbers = []
            for x in range(sx, ex + 1):
                if check(x):
                    start = end = x
                    while check(start - 1):
                        start -= 1
                    while check(end + 1):
                        end += 1
                    numbers.append(int(line[start:end+1]))
            return numbers

        numbers = find_numbers(x-1, x+1, y-1)
        numbers += find_numbers(x-1, x-1, y)
        numbers += find_numbers(x+1, x+1, y)
        numbers += find_numbers(x-1, x+1, y+1)

        if len(numbers) == 2:
            total = numbers.pop(0)
            for number in numbers:
                total *= number
            result += total

print(result)
