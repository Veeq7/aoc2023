#!/bin/python

def solution_a(lines):
    result = 0

    for line in lines:
        pass

    return result

def solution_b(lines):
    result = 0

    for line in lines:
        pass

    return result

def run(solver, filename):
    with open(filename, "r") as file:
        lines = file.readlines()

    print(f"{solver.__name__} {filename}")
    result = solver(lines)
    print(f"{result}")
    print()

if __name__ == "__main__":
    run(solution_a, "sample.txt")
    run(solution_a, "input.txt")
    run(solution_b, "sample.txt")
    run(solution_b, "input.txt")
